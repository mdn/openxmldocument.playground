﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Xml.Linq;

namespace BlueChips.OpenXml.PlayConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //CreateNewDocument();
            //OpenUpdateDocument();
            ProcessTemplateDocument();
        }

        private static void CreateNewDocument()
        {
            using (var ms = new MemoryStream())
            {
                using (var wordDoc = WordprocessingDocument.Create(ms, DocumentFormat.OpenXml.WordprocessingDocumentType.Document))
                {
                    wordDoc.AddMainDocumentPart();
                    var document = wordDoc.MainDocumentPart.Document = new Document();
                    var body = new Body();
                    document.Append(body);
                    body.Append(new Paragraph(new Run(new Text("Wow!"))));
                    body.Append(new Paragraph(new Run(new Text("Wow again!"))));

                    wordDoc.Close();
                    ms.Position = 0L;
                    var buffer = ms.ToArray();
                    File.WriteAllBytes("output.docx", buffer);
                }
            }

        }

        private static void OpenUpdateDocument()
        {
            var output = "output.docx";
            if (File.Exists(output)) File.Delete(output);
            File.Copy("Model.docx", output);
            using (var fs = File.Open(output,FileMode.Open))
            {
                using (var wordDoc = WordprocessingDocument.Open(fs,true))
                {
                    var body = wordDoc.MainDocumentPart.Document.Body;
                    body.Append(new Paragraph(new Run(new Text("Wow!"))));
                    body.Append(new Paragraph(new Run(new Text("Wow again!"))));
                    //body.

                    wordDoc.Close();

                    fs.Flush();
                }
            }

        }

        private static void ProcessTemplateDocument()
        {
            var output = "output.docx";
            if (File.Exists(output)) File.Delete(output);
            File.Copy("sample_model.docx", output);
            using (var fs = File.Open(output, FileMode.Open)) {
                using (var wordDoc = WordprocessingDocument.Open(fs, true)) {

                    //DumpXml(wordDoc, "original_document.xml");

                    var dictionary = new Dictionary<string,string>(){
                        {"ragionesociale","Cagliari SPA"},
                        {"indirizzo","Via del cappone"},
                        {"cap","2201"},
                        {"citta","Cagliari"},
                        {"referenteruolo","Luciano sigona"},
                        {"ruolo","Lucciolo"}, 
                        {"senioraccount","Casper il fantasma"},
                        {"paragrafo",GetLorem()},
                        {"strangechars",GetStrangeChars()},
                        
                        {"Title","Wow!!"},
                        {"Status","ASD!!!!"},
                        {"Company","Cispolo s.r.l."},
                        {"Company Address","Via del cappone 991"},
                        {"Author","Luciano Bitocchi"},
                        {"Comments",GetLorem()},
                        {"Category",GetStrangeChars()},
                    };

                    var processor = new DocProcessor();
                    processor.Process(wordDoc, dictionary);
                    //DumpXml(wordDoc,"output_document.xml");
                    wordDoc.Close();

                    fs.Flush();
                } 
            }
        }

        private static void DumpXml(WordprocessingDocument document,string output)
        {
            string docText;

            using (StreamReader sr = new StreamReader(document.MainDocumentPart.GetStream())) {
                docText = sr.ReadToEnd();
            }

            
            if (File.Exists(output)) File.Delete(output);

            var xml = XDocument.Parse(docText);
            xml.Save(output);

        }

        private static string GetStrangeChars()
        {

            return "\\!\"£$%&/()=?^èèòàì^ù-_<>+èòù+*@#€";
        }

        private static string GetLorem()
        {
            return @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt felis nec tellus tristique laoreet. Morbi molestie facilisis tincidunt. Nunc eu sem lectus. Aliquam blandit, metus nec volutpat scelerisque, urna libero scelerisque nisi, luctus pellentesque augue arcu quis dui. Maecenas eget rhoncus sem. Integer porttitor justo vitae neque rutrum nec semper erat gravida. Donec arcu ligula, suscipit tempor bibendum non, lacinia eget lacus. Fusce velit lectus, placerat sed posuere vitae, semper non urna. Nunc eros odio, mollis eget ornare nec, placerat eu purus. Ut sem eros, pharetra eu ullamcorper feugiat, cursus vel erat. In hac habitasse platea dictumst. Duis sed dui nisi, et tincidunt eros. Curabitur massa odio, bibendum nec luctus eget, convallis eget sem. Vestibulum iaculis, risus rutrum blandit viverra, quam velit adipiscing magna, sit amet ultrices urna turpis sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisi.



Proin ornare erat id augue tincidunt feugiat.

Sed dolor lacus, consectetur et auctor non, gravida ac eros. Vestibulum fringilla nulla in nibh pretium facilisis. In consequat faucibus posuere. Etiam id velit in lorem semper ultrices. Proin imperdiet imperdiet porttitor. Aliquam tortor elit, suscipit non adipiscing quis, lacinia eget nunc. In hac habitasse platea dictumst. Maecenas rutrum aliquet malesuada. Nulla cursus pellentesque metus eget adipiscing. Sed metus arcu, elementum quis convallis vel, ullamcorper id mi. Curabitur ac lacus vel orci molestie porttitor. Duis et vehicula risus. Integer elit dui, ultrices eget sollicitudin in, ornare congue velit. Mauris porta, elit ut egestas mollis, turpis turpis volutpat massa, in aliquam risus odio nec purus. Praesent justo lorem, laoreet ut aliquet eu, pellentesque sit amet dui.";
        }

    }
}
