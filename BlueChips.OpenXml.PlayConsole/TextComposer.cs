﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BlueChips.OpenXml.PlayConsole
{
    /// <summary>
    /// Compose text substituting palceholders
    /// </summary>
    public class TextComposer
    {

        const String RegExp = @"XX([a-zA-Z0-9_\\-]+)";

        readonly string parameterPrefix = "@@";

        readonly ComposerParameterCollection parameters = new ComposerParameterCollection();
        public ComposerParameterCollection Parameters
        {
            get { return parameters; }
        }

        Regex re;
        public Regex RE
        {
            get
            {
                re = re ?? new Regex(RegExp.Replace("XX", parameterPrefix));
                return re;
            }
        }

        public TextComposer() { }

        public TextComposer(string prefix)
        {
            parameterPrefix = prefix;
        }

        public string Process(string orginalText)
        {

            if (parameters.Count == 0) {
                return orginalText;
            }

            string text = RE.Replace(orginalText, SubstituionMatchEvaluator);

            return text;

        }

        String SubstituionMatchEvaluator(Match match)
        {
            string paramName = match.Groups[1].Value;
            if (parameters.ContainsKey(paramName)) {
                return parameters[paramName].ToString();
            }
            return match.Value;
        }


    }

    /// <summary>
    /// Collection of parameters
    /// </summary>
    public class ComposerParameterCollection : Dictionary<String, ComposerParameter>
    {
        public void Add(string name, object value)
        {
            Add(name, value, null);
        }

        public void Add(string name, object value, string format)
        {
            this[name] = new ComposerParameter() { Value = value, Format = format };
        }

        public void Add(IDictionary<string, string> values)
        {
            foreach (var elem in values) Add(elem.Key, elem.Value);
        }
    }

    /// <summary>
    /// Sobstitution parameter
    /// </summary>
    public class ComposerParameter
    {
        public Object Value { get; set; }
        public String Format { get; set; }

        public override string ToString()
        {
            if (!String.IsNullOrEmpty(Format)) {
                return String.Format(Format, Value);
            }
            if (Value == null) {
                return String.Empty;
            }

            return Value.ToString();
        }
    }
}
