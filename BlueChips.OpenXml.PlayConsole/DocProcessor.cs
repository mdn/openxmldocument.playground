﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using System.Xml.Linq;

namespace BlueChips.OpenXml.PlayConsole
{
    public class DocProcessor
    {
        public void Process(WordprocessingDocument document, Dictionary<string, string> values) {
            string docText = null;
            using (StreamReader sr = new StreamReader(document.MainDocumentPart.GetStream())) {
                docText = sr.ReadToEnd();
            }

            DumpXml(docText, "original_document.xml");


            foreach (var elem in values.ToList()) values[elem.Key] = PreprocessText(elem.Value);

            //var textComposer = new TextComposer();
            var textComposer = new OpenDocXmlComposer();

            textComposer.Parameters.Add(values);
            docText = textComposer.Process(docText);

            DumpXml(docText, "output_document.xml");

            using (StreamWriter sw = new StreamWriter(document.MainDocumentPart.GetStream(FileMode.Create))) {
                sw.Write(docText);
            }
            
        }

        private static void DumpXml(string docText, string output)
        {
            if (File.Exists(output)) File.Delete(output);

            var xml = XDocument.Parse(docText);
            xml.Save(output);
        }

        private string PreprocessText(string text)
        {
            var rows = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None)
                    .Select( t =>t.Replace("&","&amp;").Replace("<","&lt;").Replace(">","&gt;")).ToArray();
            

            return String.Join("<w:br/>",rows);
        } 
    }
}
