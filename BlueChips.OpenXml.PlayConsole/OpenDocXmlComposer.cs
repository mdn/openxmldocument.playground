﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BlueChips.OpenXml.PlayConsole
{ 
    /// <summary>
    /// Compose text substituting palceholders
    /// </summary>
    public class OpenDocXmlComposer
    {

        const String TokenRegExp = @"<\w+\:sdt>.*?<\w+\:alias[\s\w]+\:val=.([\sa-zA-Z0-9_\\-]+).\s*/>.+?<\w+\:sdtContent>(.*?)</\w+\:sdtContent>.*?</\w+\:sdt>";
        const String ReplaceTextRegExp = @"(^.*<\w+\:t>).+?(</\w+\:t>.*$)";
        const String CleanUpTextRegExp = @"<\w+\:rStyle \w+\:val=.PlaceholderText.\s*/>";


        readonly ComposerParameterCollection parameters = new ComposerParameterCollection();
        public ComposerParameterCollection Parameters
        {
            get { return parameters; }
        }

        Regex re;
        public Regex TokenRE
        {
            get
            {
                re = re ?? new Regex(TokenRegExp);
                return re;
            }
        }

        Regex _textRE;
        public Regex TextRE
        {
            get
            {
                _textRE = _textRE ?? new Regex(ReplaceTextRegExp);
                return _textRE;
            }
        }

        Regex _cleanupRE;
        public Regex CleanUpRE
        {
            get
            {
                _cleanupRE = _cleanupRE ?? new Regex(CleanUpTextRegExp);
                return _cleanupRE;
            }
        }

        public OpenDocXmlComposer() { }


        public string Process(string orginalText)
        {
             
            if (parameters.Count == 0) {
                return orginalText;
            }

            string text = TokenRE.Replace(orginalText, SubstituionMatchEvaluator);

            return text;

        }

        String SubstituionMatchEvaluator(Match match) 
        {
            string paramName = match.Groups[1].Value;
            if (parameters.ContainsKey(paramName)) {
                
                var newText = TextRE.Replace(match.Groups[2].Value, (Match innerMatch) => {
                    return innerMatch.Groups[1].Value + parameters[paramName] + innerMatch.Groups[2].Value;
                });

                newText = CleanUpRE.Replace(newText, "");

                return newText;
            }
            return String.Empty;
        }


        /// <summary>
        /// Collection of parameters
        /// </summary>
        public class ComposerParameterCollection : Dictionary<String, ComposerParameter>
        {
            public void Add(string name, object value)
            {
                Add(name, value, null);
            }

            public void Add(string name, object value, string format)
            {
                this[name] = new ComposerParameter() { Value = value, Format = format };
            }

            public void Add(IDictionary<string, string> values)
            {
                foreach (var elem in values) Add(elem.Key, elem.Value);
            }
        }

        /// <summary>
        /// Sobstitution parameter
        /// </summary>
        public class ComposerParameter
        {
            public Object Value { get; set; }
            public String Format { get; set; }

            public override string ToString()
            {
                if (!String.IsNullOrEmpty(Format)) {
                    return String.Format(Format, Value);
                }
                if (Value == null) {
                    return String.Empty;
                }

                return Value.ToString();
            }
        }

    }

    
}
